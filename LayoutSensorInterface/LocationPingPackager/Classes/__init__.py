"""This package contains all the worker thread classes that are needed by the
   BDCS program. The threads communicate data via python Queue objects and with
   the exception of the sniffer and sender classes, each will only have one
   instance.
   These threads should be treated as classes rather than modules.
"""
