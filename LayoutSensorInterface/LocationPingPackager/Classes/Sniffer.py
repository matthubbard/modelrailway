#!/usr/bin/python2
"""   A generic sniffer class that can be instanced for each server address
      needed by the BDCS.
"""

import threading
import socket
import time

#------------------------------------------------------------------------------

class Sniffer(threading.Thread):
   def __init__(self, print_lock, ip, port, queue, timeout, keep_msg_saddr_pairs=False):
      threading.Thread.__init__(self)
      self.print_lock = print_lock
      self.ip = ip
      self.port = port
      self.queue = queue
      self.running = True
      self.data = ""
      self.timeout = timeout
      self.keep_msg_saddr_pairs = keep_msg_saddr_pairs

   def run(self):
      print "| Listening at ip/port: %s, %s" % (self.ip, self.port)
      
      self.sock = socket.socket(socket.AF_INET,   # Internet
                          socket.SOCK_DGRAM)      # UDP
      # Mainly in case we forget to initialise the addresses correctly we don't
      # want the threads to die and have to restart the app.
      bound = False
      error_printed = False
      while bound == False:
         try:
            self.sock.bind((self.ip, self.port))
            bound = True
         except socket.error, e:
            if e.errno == 10049 and error_printed == False:
               with self.print_lock:
                  print '| %s @ %s:%s. BDCS trying to bind...' % (e, self.ip, self.port)
               error_printed = True
            else:
               pass
         self.sock.settimeout(self.timeout)

      while self.running:
         try:
            self.data, (saddr, sport) = self.sock.recvfrom(65507) # buffer
            # This should only be true for the OBMS messages so that we can
            # track their origin but actually could be better to do it for all
            # message so that we can do away with the idea of using a dwitch to
            # go between local and the integrated network setup.
            if self.keep_msg_saddr_pairs == True:
               msg_saddr = (self.data, saddr)
               self.queue.put(msg_saddr)
            else:
               self.queue.put(self.data)
            with self.print_lock:
               print "| Received message at %s from %s:%s:\n" % (time.strftime('%X'), 
                                                               saddr, sport), self.data
         except socket.timeout:
            pass

   def stop(self):
      self.running = False
      with self.print_lock:
         print "| Closing socket at ip/port: %s, %s" % (self.ip, self.port)
      self.sock.close() 
