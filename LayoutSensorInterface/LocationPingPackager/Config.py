#!/usr/bin/python
#------------------------------------------------------------------------------
""" 

"""
#------------------------------------------------------------------------------
import serial
from Queue import Queue

#------------------------------------------------------------------------------

COMMAND_SEPARATOR = ","


class CommsParams():
   """ Needs more structure, even if only for readability.
       Need to also think about using PriorityQueue to add some more control,
       not always appropriate though.
   """
   NAME = 'LocationPingPackager'

   # Set the default scenario and initial train position by occupied sensor
   # number.
   SCENARIO = "Up Oxford" # "Up Main", "Up Oxford"
   SCENARIO_SETUP = {
      "Up Oxford": {
         "Start Position 1": 0,
         "Start Poistion 2": -1
      },
      "Up Main": {
         "Start Position 1": 18,
         "Start Poistion 2": -1
      },
   }

   # Either 'TCP/IP' or 'UDP'
   NETWORK_PROTOCOL = 'TCP/IP'
   # Local mode sets all IP addresses to LOCALHOST when True.
   LOCAL_MODE = False  # { True | False }
   # Set to True to enable debug stdout - not implemented need to stick
   # condition on all print statements
   DEBUG_MODE = True
   # Stubbed input mode to ignore serial init.
   TEST_MODE = False

   # Serial port options
   SPORT_NAMES = ['\\.\COM3']  # TODO ['\\.\COM1', '\\.\COM2']
   SPORTS = []
   if not TEST_MODE:
      for sport_name in SPORT_NAMES:
         SPORTS.append(
            serial.Serial(
               port=sport_name,
               baudrate=9600,
               parity=serial.PARITY_ODD,
               stopbits=serial.STOPBITS_TWO,
               bytesize=serial.SEVENBITS,
               timeout=5))

   # The number of clients that will be in the system
   NUM_CLIENTS = 1
   # The number of servers that we report to
   NUM_SERVERS = len(SPORT_NAMES)  # TODO len(SPORT_NAMES)
   # For network clients
   PORT_CLIENT = 10000
   # ...add more when needed
   # LOCALHOST IP
   LOCALHOST = "127.0.0.1"
   # The Server ID
   SERVER_ID = NAME + '@Altran'

   # client address is either LOCALHOST or is assigned incrementally
   # 192.168.1.151++.
   IP_CLIENTS = []
   if LOCAL_MODE:
      for i in range(0, NUM_CLIENTS):
         IP_CLIENTS.append("127.0.0.%s" % (i + 1))
      IP_SERVER = LOCALHOST
   else:
      if NUM_CLIENTS < 8:
         for i in range(0, NUM_CLIENTS):
            IP_CLIENTS.append("192.168.1.5%s" % (i))
      # Dont care for a Serial input IP_SERVER = "192.168.1.51"

   # We define Queues here for data sharing between threads. Should have
   # maxsizes although we are being careful you never know.
   QUEUES_IN = []
   QUEUES_OUT = []
   for i in range(0, NUM_SERVERS):
      QUEUES_IN.append(Queue())
   for i in range(0, NUM_CLIENTS):
      QUEUES_OUT.append(Queue())

   #

   # The TIMEOUT of the sniffers
   TIMEOUT = 600


#------------------------------------------------------------------------------


class MessageTemplates():
   """   Static methods for when we want to create a message from a template.
   """

   @staticmethod
   def make_ModelRailwayPositionData(utcTime, NMEA_lat, NSEW_lat, NMEA_long,
                                     NSEW_long, sog, hAcc):
      """ This will return a ModelRailwayPositionData message - see the Java
          for latest structure.

          --header
            COMMAND + " " + PAYLOAD_LENGTH + " " +
          --payload
            utcTime + "," + coordinate.getNMEALatitude() + "," + 
               coordinate.getNMEALongitude() + "," + sog + "," + hAcc
          e.g.
          MODEL_RAILWAY_POSITION_DATA 45 161633.00,5136.74381,N,00114.81442,W,36.0,0.0

      """
      payload = "%s%s%s%s%s%s%s%s%s%s%s%s%s" % (utcTime, COMMAND_SEPARATOR,
                                                NMEA_lat, COMMAND_SEPARATOR,
                                                NSEW_lat, COMMAND_SEPARATOR,
                                                NMEA_long, COMMAND_SEPARATOR,
                                                NSEW_long, COMMAND_SEPARATOR,
                                                sog, COMMAND_SEPARATOR, hAcc)
      payload_length = len(payload)
      return "MODEL_RAILWAY_POSITION_DATA %s %s" % (payload_length, payload)

   @staticmethod
   def make_Reset():
      """ This will return a Reset message - see the Java
          for latest structure.

          --header
            COMMAND + " " + PAYLOAD_LENGTH + " " +
          --payload
            "RESET"
          e.g.
          RESET 5 RESET

      """
      return "RESET 5 RESET" + chr(10)

class SensorMap():
   """
   """

   def __init__(self):
      self.SENSOR_MAP = {}
      self.init_sensor_map()

   def init_sensor_map(self):
      """ See S.P1660.250.2 Design
      
          TODO put this in the design!!:
          In order to determine when a new occupation is erroneous we need to know if there
          are adjacents in both directions that have an Occupied or wasOccupied state.
      """
      # <AUTO-GEN BEGIN>
      self.SENSOR_MAP[0] = {
         "NMEA_lat": " 51.673517  ",
         "NMEA_long": "  -1.240985  ",
         "Adjacent": {
            "Forward"  : [1],
            "Backward" : []
            }
      }
      self.SENSOR_MAP[1] = {
         "NMEA_lat": " 51.644883  ",
         "NMEA_long": "  -1.239834  ",
         "Adjacent": {
            "Forward"  : [2],
            "Backward" : [0]
            }
      }
      self.SENSOR_MAP[2] = {
         "NMEA_lat": " 51.641369  ",
         "NMEA_long": "  -1.241594  ",
         "Adjacent": {
            "Forward"  : [3],
            "Backward" : [1]
            }
      }
      self.SENSOR_MAP[3] = {
         "NMEA_lat": " 51.637814  ",
         "NMEA_long": "  -1.242821  ",
         "Adjacent": {
            "Forward"  : [4],
            "Backward" : [2]
            }
      }
      self.SENSOR_MAP[4] = {
         "NMEA_lat": " 51.634247  ",
         "NMEA_long": "  -1.243264  ",
         "Adjacent": {
            "Forward"  : [5],
            "Backward" : [3]
            }
      }
      self.SENSOR_MAP[5] = {
         "NMEA_lat": " 51.630678  ",
         "NMEA_long": "  -1.243159  ",
         "Adjacent": {
            "Forward"  : [6],
            "Backward" : [4]
            }
      }
      self.SENSOR_MAP[6] = {
         "NMEA_lat": " 51.627055  ",
         "NMEA_long": "  -1.24297  ",
         "Adjacent": {
            "Forward"  : [7],
            "Backward" : [5]
            }
      }
      self.SENSOR_MAP[7] = {
         "NMEA_lat": " 51.623526  ",
         "NMEA_long": "  -1.243764  ",
         "Adjacent": {
            "Forward"  : [8,49],
            "Backward" : [6]
            }
      }
      self.SENSOR_MAP[8] = {
         "NMEA_lat": " 51.621125  ",
         "NMEA_long": "  -1.244535  ",
         "Adjacent": {
            "Forward"  : [9],
            "Backward" : [7]
            }
      }
      self.SENSOR_MAP[9] = {
         "NMEA_lat": " 51.619655  ",
         "NMEA_long": "  -1.245448  ",
         "Adjacent": {
            "Forward"  : [8,46],
            "Backward" : [10]
            }
      }
      self.SENSOR_MAP[10] = {
         "NMEA_lat": " 51.617495  ",
         "NMEA_long": "  -1.246761  ",
         "Adjacent": {
            "Forward"  : [9],
            "Backward" : [11]
            }
      }
      self.SENSOR_MAP[11] = {
         "NMEA_lat": " 51.615828  ",
         "NMEA_long": "  -1.247114  ",
         "Adjacent": {
            "Forward"  : [10],
            "Backward" : [12]
            }
      }
      self.SENSOR_MAP[12] = {
         "NMEA_lat": "51.614427",
         "NMEA_long": "-1.251513",
         "Adjacent": {
            "Forward"  : [11],
            "Backward" : [13,20]
            }
      }
      self.SENSOR_MAP[13] = {
         "NMEA_lat": " 51.614539  ",
         "NMEA_long": "  -1.254444  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : []
            }
      }
      self.SENSOR_MAP[14] = {
         "NMEA_lat": " 51.615018  ",
         "NMEA_long": "  -1.256514  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : []
            }
      }
      self.SENSOR_MAP[17] = {
         "NMEA_lat": " 51.61935  ",
         "NMEA_long": "  -1.280798  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : [45]
            }
      }
      self.SENSOR_MAP[18] = {
         "NMEA_lat": " 51.619315  ",
         "NMEA_long": "  -1.280804  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : [32]
            }
      }
      self.SENSOR_MAP[19] = {
         "NMEA_lat": " 51.619271  ",
         "NMEA_long": "  -1.280804  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : [22]
            }
      }
      self.SENSOR_MAP[20] = {
         "NMEA_lat": " 51.614504  ",
         "NMEA_long": "  -1.254445  ",
         "Adjacent": {
            "Forward"  : [45,32],
            "Backward" : [12,25]
            }
      }
      self.SENSOR_MAP[21] = {
         "NMEA_lat": " 51.614468  ",
         "NMEA_long": "  -1.254445  ",
         "Adjacent": {
            "Forward"  : [34],
            "Backward" : [33]
            }
      }
      self.SENSOR_MAP[22] = {
         "NMEA_lat": " 51.61441  ",
         "NMEA_long": "  -1.254502  ",
         "Adjacent": {
            "Forward"  : [19],
            "Backward" : []
            }
      }
      self.SENSOR_MAP[24] = {
         "NMEA_lat": " 51.6123969  ",
         "NMEA_long": "  -1.2469070  ",
         "Adjacent": {
            "Forward"  : [35],
            "Backward" : [34]
            }
      }
      self.SENSOR_MAP[25] = {
         "NMEA_lat": " 51.6123969  ",
         "NMEA_long": "  -1.2469070  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : []
            }
      }
      self.SENSOR_MAP[30] = {
         "NMEA_lat": " 51.5958724  ",
         "NMEA_long": "  -1.1977050  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : [36]
            }
      }
      self.SENSOR_MAP[32] = {
         "NMEA_lat": " 51.615401  ",
         "NMEA_long": "  -1.258237  ",
         "Adjacent": {
            "Forward"  : [18],
            "Backward" : [20,33]
            }
      }
      self.SENSOR_MAP[33] = {
         "NMEA_lat": " 51.614642  ",
         "NMEA_long": "  -1.255183  ",
         "Adjacent": {
            "Forward"  : [21],
            "Backward" : [19,32]
            }
      }
      self.SENSOR_MAP[34] = {
         "NMEA_lat": " 51.613451  ",
         "NMEA_long": "  -1.250594  ",
         "Adjacent": {
            "Forward"  : [24],
            "Backward" : [21]
            }
      }
      self.SENSOR_MAP[35] = {
         "NMEA_lat": " 51.601599  ",
         "NMEA_long": "  -1.216710  ",
         "Adjacent": {
            "Forward"  : [36,31],
            "Backward" : [24]
            }
      }
      self.SENSOR_MAP[36] = {
         "NMEA_lat": " 51.6006671  ",
         "NMEA_long": "  -1.2109158  ",
         "Adjacent": {
            "Forward"  : [30],
            "Backward" : [50, 25, 35]
            }
      }
      self.SENSOR_MAP[37] = {
         "NMEA_lat": " 51.641374  ",
         "NMEA_long": "  -1.241649  ",
         "Adjacent": {
            "Forward"  : [46],
            "Backward" : []
            }
      }
      self.SENSOR_MAP[45] = {
         "NMEA_lat": " 51.615018  ",
         "NMEA_long": "  -1.256514  ",
         "Adjacent": {
            "Forward"  : [17],
            "Backward" : [20]
            }
      }
      self.SENSOR_MAP[46] = {
         "NMEA_lat": " 51.619007  ",
         "NMEA_long": "  -1.245566  ",
         "Adjacent": {
            "Forward"  : [47],
            "Backward" : [37]
            }
      }
      self.SENSOR_MAP[47] = {
         "NMEA_lat": " 51.617503  ",
         "NMEA_long": "  -1.246798  ",
         "Adjacent": {
            "Forward"  : [48],
            "Backward" : [46]
            }
      }
      self.SENSOR_MAP[48] = {
         "NMEA_lat": " 51.620068  ",
         "NMEA_long": "  -1.246416  ",
         "Adjacent": {
            "Forward"  : [13],
            "Backward" : [47]
            }
      }
      self.SENSOR_MAP[49] = {
         "NMEA_lat": " 51.617792  ",
         "NMEA_long": "  -1.243004  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : [7]
            }
      }
      self.SENSOR_MAP[50] = {
         "NMEA_lat": " 51.5958724  ",
         "NMEA_long": "  -1.1977050  ",
         "Adjacent": {
            "Forward"  : [],
            "Backward" : []
            }
      }
      # <AUTO-GEN END>

      # Clean it up
      for key in self.SENSOR_MAP:
         for val in self.SENSOR_MAP[key]:
            if val == "NMEA_lat" or val == "NMEA_long":
               self.SENSOR_MAP[key][val] = self.SENSOR_MAP[key][val].replace(
                  " ", "")

   def get_instance(self):
      return self.SENSOR_MAP
