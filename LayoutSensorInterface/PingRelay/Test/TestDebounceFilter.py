from datetime import datetime
from datetime import timedelta
import time

# Set up test variables
prevRead=1
read=1
pings=0
averageTimeout = 0.5
pingThreshold = 3

def monitorDebounced(read, prevRead, pings):
  """ High pass filter that only allows a signals through where more than the
      threshold amount of pings are detected in the average timeout window.

      By counting the number of pings in a short timeframe like this we can 
      eliminate some random noise that occurs with the reed sensors. The noise
      is rare but known for at least sensor 17 - probably due to crosstalk.

      The noise induced by turning on the manual point switching caps is so
      prolific and fast that this kind of filter may not correct the behaviour
      as the arduino will still detect many pings in a very short amount of
      time.
  """
  count = 0
  timerStart = datetime.now()

  for i in range(0, 30):
    read = genPing(read, pings);
    pings += 1
    if read == None:
      break
    
    if (read != prevRead):
      if (datetime.now() < timerStart + timedelta(seconds=averageTimeout)):
        count+=1
        print
        print "Timer Start : ", timerStart
        print "Current Time: ", datetime.now()
        print "Count       : ", count
        print

      elif (count >= pingThreshold):
        print "####---------------------------------------"
        print "#### PING @ ", datetime.now()
        print "####---------------------------------------"
        count = 0
        timerStart = datetime.now()
      else:
        count = 0
        timerStart = datetime.now()

      # print "Timer start:  ", timerStart
    prevRead = read

def genPing(read, pings):
  """ Alternate and gen 10 pings

      For the first 10 pings have eveny spaced every 0.15 seconds so that we
      would only get 3 in a 0.5 second.

      The next 10 pings all happen within 0.5 seconds so this should certainly
      be a read.

      The next 10 pings will be in groups of 2 - 3 - 3 - 1 - 1

  """
  if pings < 10:
    time.sleep(0.15)
    print "0.15 sleep"
  elif pings < 11:
    time.sleep(averageTimeout + 0.1)
    print "long sleep"
  elif pings < 20:
    time.sleep(0.001)
    print "0.001 sleep"
  elif pings < 21:
    time.sleep(averageTimeout + 0.1)
    print "long sleep"
  elif pings < 22:
    pass
    print "no sleep"
  elif pings < 23:
    time.sleep(averageTimeout + 0.1)
    print "long sleep"
  elif pings < 25:
    pass
    print "no sleep"
  elif pings < 26:
    time.sleep(averageTimeout + 0.1)
    print "long sleep"
  elif pings < 28:
    pass
    print "no sleep"
  elif pings < 30:
    time.sleep(averageTimeout + 0.1)
    print "long sleep"
  # Alternate the reads between HIGH and LOW
  if read == 1:
    return 0
  else:
    return 1


monitorDebounced(read, prevRead, pings)