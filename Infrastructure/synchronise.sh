#!/bin/sh

# syncs (push) the current dir into PIDIR

WINDIR=./Remote
PIDIR=/home/pi/Remote

PIIP1=192.168.1.51
PIIP2=192.168.1.52

PIUSER=pi

rsync -ahP $WINDIR/ $PIUSER@$PIIP:$PIDIR
